from influxdb_client import InfluxDBClient, Point, WriteOptions
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime
import random
from time import sleep
from pytz import UTC
from fluxquerybuilder import FluxQueryBuilder, Logics

def generate_pa(write_api):
    for i in range(100):
        points = []
        for j in range(10):
            point = Point("sensors").tag("machine_id", "1") \
                .field("pressure1", random.randint(0, 50)) \
                .field("pressure2", random.randint(0, 50)) \
                .field("pressure3", 22.34 if bool(random.getrandbits(1)) else None) \
                .field("pressure4", 5.162) \
                .time(datetime.now(UTC))

            points.append(point)
            sleep(0.001)
        write_api.write("default", record=points)
        sleep(0.02)
        print((i + 1) * j)


def rand_nullable_int():
    nums = [None, random.randint(0, 100)]
    return random.choice(nums)


def generate_fabforce_data(write_api):
    tools_id = ["1", "2", "3"]
    run_id = 563266666516
    machine_id = ["1", "2", "3"]
    model_id = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20]

    print("Start: ", datetime.now(UTC))
    for i in range(100):
        points = []
        for j in range(100):
            point = Point("sensors")\
                .tag("machine_id", random.choice(machine_id)) \
                .tag("tool_id", random.choice(tools_id)) \
                .field("run_id", str(run_id))\
                .time(datetime.now(UTC))
            for id in model_id:
                point.field(f's{id}', rand_nullable_int())
            points.append(point)

            sleep(0.001)
        run_id += 1
        write_api.write("default", record=points)
        if i % 5 == 0:
            sleep(0.02)
        else:
            sleep(0.5)
        print((i + 1) * j)
    print("End: ", datetime.now(UTC))

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    INFLUXDB_V2_TOKEN = "73A1Fz9Ck7-x85BuXpgVce-MWwuGiVfO4k-SilvhRJpRLAq4xbO_zhZx-nbyoxcehB9a_aXoAKb1UQq-x_Ly7g=="
    INFLUXDB_V2_ORG = "ff"

    tocken_pa = "00lh_Frbtw5JthC4fyvht9R6nRB7f8X0YjTGKA2n145XuD9gQI-i56ppqh6ps83j-oNUQm01k-kR_5Xp4hqELg=="
    org_pa = "pa"

    client = InfluxDBClient(url="http://localhost:8086", token=INFLUXDB_V2_TOKEN, org=INFLUXDB_V2_ORG)

    write_api = client.write_api(write_options=SYNCHRONOUS)
    query_api = client.query_api()

    generate_fabforce_data(write_api)
    #generate_pa(write_api)
    # query = "from(bucket: \"bucket1\")\r\n  |> range(start: -1d)\r\n  " \
    #         "|> filter(fn: (r) => r[\"_measurement\"] == \"force\")\r\n  " \
    #         "|> filter(fn: (r) => r[\"machine_id\"] == \"1\" and r[\"tools_id\"] == \"1\")\r\n  " \
    #         "|> pivot(rowKey:[\"_time\"], columnKey: [\"_field\"], valueColumn: \"_value\")"
    #
    #
    # data_frame = query_api.query_data_frame(query)
    # print(data_frame.head(10))

    # filters = {
    #     "machine_id": ["1"],
    #     "tools_id": ["1", "2"]
    # }
    # query1 = FluxQueryBuilder("bucket1", "sensor").range("-1d").filter(filters, Logics.And). \
    #     pivot(row_key=["_time"], column_key=["_field"], value_column="_value").\
    #     group_by(["tools_id"]).\
    #     build()
    # print(query1)
    # data_frame1 = query_api.query_data_frame(query1)
    # # print(data_frame1.head())
    # print(type(data_frame1))


    write_api.close()
    client.close()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
