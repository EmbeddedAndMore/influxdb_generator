from __future__ import annotations

from typing import Optional, Dict, List, Union
from enum import Enum, auto

__all__ = ('FluxQueryBuilder', 'Logics',)


class FunctionOrderError(Exception):
    pass


class Logics(Enum):
    And = auto()
    Or = auto()
    Greater = auto()
    Smaller = auto()


class FluxQueryBuilder:

    def __add__(self, other: FluxQueryBuilder) -> FluxQueryBuilder:
        ...

    def __init__(self, bucket: str, measurement: str):
        self.__script = f'from(bucket: "{bucket}")'
        self.__pipe = '\n  |> '
        self.__measurement = measurement
        self.__ranged = False
        self.__filtered = False
        self.__pivoted = False

    def range(self, start: str, stop: Optional[str] = None) -> FluxQueryBuilder:

        if start is None:
            raise ValueError('\"start\" can not be None')

        if stop:
            if start.isdigit() and stop.isdigit() and int(stop) <= int(start):
                raise ValueError('\"stop\" should be greater than \"start\"')

        self.__script += self.__pipe + f"range(start:{start}"

        if stop:
            self.__script += f", stop:{stop}"
        self.__script += ")"

        # always set this filter
        self.__script += self.__pipe + f'filter(fn: (r) => r["_measurement"] == "{self.__measurement}")'

        self.__ranged = True
        return self

    def filter(self, filters: Dict[str, Union[str, int, list]], logic: Logics) -> FluxQueryBuilder:
        if filters is None or len(filters) == 0:
            raise ValueError('\"filters\" can not be None or empty')

        if not self.__ranged:
            raise FunctionOrderError('"range()" should be called before')

        if not self.__valid_list(list(filters.keys())):
            return self

        self.__script += self.__pipe + f'filter(fn: (r) => '
        for key, value in filters.items():
            if value is None or not key:
                continue
            if isinstance(value, list):
                self.__script += self.__create_contains(key, value)
            else:
                self.__script += f'r["{key}"] == {self.__cq(value)}'
            if logic == Logics.And:
                self.__script += " and "
            else:
                self.__script += " or "

        self.__script = self.__script[:-4] + ")"
        self.__filtered = True
        return self

    def group_by(self, columns: Optional[List[str]] = None) -> FluxQueryBuilder:

        if not self.__valid_list(columns):
            return self

        if not self.__filtered:
            raise FunctionOrderError('"filter()" should be called before')

        self.__script += self.__pipe + 'group(columns: '
        self.__script += self.__string_list(values=columns)
        self.__script += ")"
        return self

    def limit(self, n: int) -> FluxQueryBuilder:
        if n is None:
            return self

        if not self.__filtered:
            raise FunctionOrderError('"filter()" should be called before')

        self.__script += self.__pipe + f'limit(n: {n})'
        return self

    def drop(self, columns: Optional[List[str]] = None) -> FluxQueryBuilder:

        if not self.__valid_list(columns):
            return self

        if not self.__filtered:
            raise FunctionOrderError('"filter()" should be called before')

        self.__script += self.__pipe + 'drop(columns: '
        self.__script += self.__string_list(values=columns)
        self.__script += ")"
        return self

    def pivot(self, row_key: List[str], column_key: List[str], value_column: str) -> FluxQueryBuilder:

        if not (self.__valid_list(row_key) and self.__valid_list(column_key)):
            raise ValueError("Wrong input parameter")
        if not value_column:
            raise ValueError("Wrong input parameter")

        row_key = self.__string_list(values=row_key)
        column_key = self.__string_list(values=column_key)

        self.__script += self.__pipe + f'pivot(rowKey:{row_key}, columnKey: {column_key}, valueColumn: "{value_column}")'
        self.__pivoted = True
        return self

    def limit_columns(self, columns: List[str]) -> FluxQueryBuilder:
        if not self.__valid_list(columns):
            raise ValueError("Columns list is not contains None or Empty string")

        if self.__pivoted:
            raise ValueError("Cannot limit columns after pivot")

        self.filter({"_field": columns}, Logics.And)
        return self

    def build(self) -> str:
        if not self.__filtered:
            raise FunctionOrderError('"filter()" should be called before')
        return self.__script

    def __cq(cls, val: Union[str, int]):
        if isinstance(val, int):
            return val
        return f'"{val}"'

    def __valid_list(cls, values: List[str]):
        if values is None or len(values) == 0:
            return False
        elif all(not val for val in values):
            return False
        return True

    def __string_list(self, values: List[str]) -> str:
        str_list = '['
        for val in values:
            if not val:
                continue
            str_list += self.__cq(val) + ', '

        return str_list[:-2] + ']'

    def __create_contains(self, column: str, values: List[Union[str, int, float, bool]]):

        if all(isinstance(x, str) for x in values):
            values = self.__string_list(values)
        else:
            values = [x for x in values if x is not None]
        return f'contains(value: r["{column}"], set: {values})'
